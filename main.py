import random

from tachando_digitos.algorythm import Algorythm

WELCOME_TEXT = """
Bienvenido al generador de tableros de "Sutoreto" y "Tachando Digitos".
"""

OPTION_TEXT = """
Escoja el tipo de tablero que quiere generar por lotes.
1 - Sutoreto.
2 - Tachando Digitos.
q - Salir.
"""

BATCH_TEXT = "Cantidad de tableros\n"
TABLE_TEXT = "Tamano del tablero\n"

def generate_sutoreto_tables(batch: int, table_size: int) -> None:
    print("Generating sutoreto tables")
    import sutoreto.algorythm
    algo = Algorythm()
    tables = algo.generate_batch(batch, table_size)
    algo.save_tables(tables)
    pass

def generate_tachando_digitos_tables (batch: int, table_size: int) -> None:
    print("Generating tachando digitos tables")
    import tachando_digitos.algorythm
    algo = Algorythm()
    tables = algo.generate_batch(batch, table_size)
    algo.save_tables(tables)
    pass

if __name__ == '__main__':
    print(WELCOME_TEXT)

    method = None
    while True:
        option = input(OPTION_TEXT)
        if option == '1':
            method =  generate_sutoreto_tables
            pass
        elif option == '2':
            method = generate_tachando_digitos_tables
            pass
        elif option == 'q':
            break
        else:
            continue

        batch: int = int(input(BATCH_TEXT))
        table_size: int = int(input(TABLE_TEXT))
        method(batch, table_size)

