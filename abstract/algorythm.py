from abstract.table import Table

class Algorythm:

    def __init__(self):
        pass
    
    def split_table_rows (self, table: Table) -> list:
        l = []
        for n in range (table.size):
            l.append (table.get_sublist_from_row(n, 0, table.size - 1))
        return l
    
    def split_table_cols (self, table: Table) -> list:
        l = []
        for n in range (table.size):
            l.append (table.get_sublist_from_col(n, 0, table.size - 1))
        return l
    
    def config_table(self, table: Table) -> Table:
        """
            Receive a table with the solution, make a copy and it
            gets decorated with random values.
        """
        return None
    
    def save_tables(self, tables: list) -> bool:
        """
        Convert the table content to a json and save it in a file.
        """
        return True

    def generate (self, table_size: int) -> tuple:
        raise NotImplementedError
    
    def generate_batch (self, batch: int, table_size: int, known_table: list = []) -> list:
        batch_counter: int = 0
        l = []
        while batch_counter < batch:
            ts: tuple = self.generate(table_size)
            if not self.exists(ts[1], known_table):
                batch_counter += 1
                l.append(ts)
                known_table.append(ts[1])
        return l
    
    def exists(self, table: Table, tables: list) -> bool:
        for t in tables:
            if table == t:
                return True
        return False

    def load_tables(self) -> list:
        """
        Load tables from json file
        """