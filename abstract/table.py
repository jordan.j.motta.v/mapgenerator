class Table:
    """
    Generic tables with basic implementation to handle the matrix.

    cells -> List
    """

    def __init__(self, size: int, cls):
        self._cells = []
        for _ in range (size * size):
            self._cells.append (cls())
        self._size = size
        

    def get_sublist_from_col (self, col: int, begin: int, end: int) -> list:
        """
        IMPORTANT: 'begin' must always be less than 'end'.
        """
        if begin > end:
            return []
        if end > self.size:
            return []
        
        l = []
        diff: int = end - begin
        cs = self.cells
        for n in range (diff + 1):
            c = cs[n * self.size + col]
            l.append (c)
        
        return l

    def get_sublist_from_row (self, row: int, begin: int, end: int) -> list:
        """
        IMPORTANT: 'begin' must always be less than 'end'.
        """
        if begin > end:
            return []
        if end > self.size:
            return []
        
        r = row * self.size
        l = self.cells[r + begin: r + end + 1]
        return l

    
    def _get_cell (self, row: int, col: int):
        conditions = row >= self.size and row < 0 and col >= self.size and col < 0
        if conditions:
            return None
        return self._cells[row * self.size + col]

    def get_cell (self, row: int, col: int):
        return self._get_cell (row, col)


    def _get_cells (self):
        return self._cells
    
    cells = property (_get_cells)

    def _get_size (self) -> int:
        return self._size

    size = property(_get_size)

    def get_coord (self, cell) -> tuple:
        # index: int = self._cells.index(cell)
        for i in range(len(self._cells)):
            if cell is self._cells[i]:
                index = i
                break
        col = index % self.size
        row = int(index / self.size)
        return (row, col)

    def pretty_print (self):
        for row in range (self._size):
            srow = ''
            for col in range (self._size):
                srow += ' | ' + str(self._cells[row * self._size + col].value)
            srow += ' |'
            print(srow)
    
    def cells_to_dict(self, *props) -> list:
        """
            returns a dictionary with the properties specified by "props"

            params
            props: dict
                Properties which will be saved in the dictionary.
                IMPORTANT: The property must exists in the table. 
        """

        cs = []
        for c in self._cells:
            vals: dict = {}
            for key in props:
                vals[key] = getattr(c, key)
            cs.append(vals)
        
        return cs