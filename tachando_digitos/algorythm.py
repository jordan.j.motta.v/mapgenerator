import random
import copy
import os
from tachando_digitos.table import Table
from tachando_digitos.cell import Cell
from abstract import algorythm
import json

class Algorythm(algorythm.Algorythm):

    def __init__ (self):
        pass

    def _count_selected_cells (self, sublist: list) -> int:
        count: int = 0
        for c in sublist:
            if c.selected == True:
                count += 1
        return count
    

    def fill_col (self, table: Table, index: int) -> int:
        sublist: list = table.get_sublist_from_col(index, 0, table.size - 1)
        elements: int = random.randint (1,int( table.size / 2))
        selected_cells: int = self._count_selected_cells (sublist)

        if (elements - selected_cells) <= 0:
            return selected_cells
        
        l = []
        for _ in range (elements):
            i: int = random.randint (0, table.size - 1)
            value: int = random.randint (1, 50)
            l.append ((i, value))

        table.fill_sublist (index, l, table.get_sublist_from_col)
        count: int = self._count_selected_cells (sublist)
        return count
        
    def fill_row (self, table: Table, index: int) -> int:

        sublist: list = table.get_sublist_from_row(index, 0, table.size - 1)
        elements: int = random.randint (1,int( table.size / 2))
        selected_cells: int = self._count_selected_cells (sublist)

        if (elements - selected_cells) <= 0:
            return self._count_selected_cells (sublist)
        
        l = []
        for _ in range (elements):
            i: int = random.randint (0, table.size - 1)
            value: int = random.randint (1, 50)
            l.append ((i, value))

        table.fill_sublist (index, l, table.get_sublist_from_row)
        count: int = self._count_selected_cells (sublist)
        return count

    def fill_sublist (self, t: Table, sublist_method, index: int) -> int:
        """
        This method pick up some cells in sublist and assign it some numbers,
        then the sum of those numbers will be in the side_list.

        To sublist will be assigned less than half of the size of it.

        returns how many elements were selected
        """

    
        sublist = getattr (t,sublist_method.__name__)(index, 0, t.size - 1)
        elements: int = random.randint (1,int( t.size / 2))
        selected_cells: int = self._count_selected_cells (sublist)

        if (elements - selected_cells) <= 0:
            return

        l = []
        for _ in range (elements):
            index: int = random.randint (0, t.size - 1)
            value: int = random.randint (1, 50)
            l.append ((index, value))

        count: int = self._count_selected_cells (sublist)
        return count
    
    def sum_side_list (self, sublist: list, side_list: list, index: int) -> None:
        sum: int = 0
        for c in sublist:
            if c.selected:
                sum += c.value
        
        side_list[index] = sum
    
    def config_table(self, table) -> Table:
        copy_table: Table = copy.deepcopy(table)

        (min, max) = self._min_max(copy_table)

        for c in copy_table.cells:
            if not c.selected:
                c.value = random.randint(min, max)
        return copy_table
    
    def _min_max(self, table: Table) -> tuple:
        min: int = 99
        max: int = 0
        for c in table.cells:
            if c.selected:
                if c.value > max:
                    max = c.value
                elif c.value < min:
                    min = c.value
        return (min, max)
        
    def save_tables(self, tables) -> bool:

        with open('tachando_digitos_tables.json', 'a') as outfile:
            l = []
            for t in tables:
                t0: Table = t[0]
                cs0: list = t0.cells_to_dict('selected', 'value')
                ls = t0.left_list
                lt = t0.top_list
                t0vals: dict = {
                    'cells': cs0,
                    'left_list': ls,
                    'top_list': lt
                }
                t1: Table = t[1]
                cs1 = t1.cells_to_dict('selected', 'value')
                ls = t1.left_list
                lt = t1.top_list
                t1vals: dict = {
                    'cells': cs1,
                    'left_list': ls,
                    'top_list': lt
                }

                vals = {
                    'answer_table': t0vals,
                    'complete_table': t1vals
                }
                l.append(vals)
            json.dump(l, outfile)
        return True

    def generate(self, size: int) -> tuple:
        answer_table: Table = Table(size)
        for n in range(size):
            self.fill_col (answer_table, n)
            self.fill_row (answer_table, n)
        answer_table.apply_results ()

        complete_table: Table = self.config_table(answer_table)
        return (answer_table, complete_table)
     
    def load_tables(self) -> list:
        """
        Load tables from json file
        """
        filename: str = 'tachando_digitos_tables.json'
        known_tables: list = []
        if not os.path.exists(filename):
            return known_tables
        with open(filename) as output:
            tables: list = json.load(output)
            for ca_table in tables:
                ct: Table = ca_table['complete_table']
                # Get table size
                left_list: list = ct['left_list']
                table_size: int = len(left_list)
                table: Table = Table(table_size)
                # Fill left list
                for index in range(table_size):
                    table.left_list[index] = left_list[index]
                # Fill left list
                top_list: list = ct['top_list']
                for index in range(table_size):
                    table.top_list[index] = top_list[index]
                # Fill cells
                cells: list = ct['cells']
                for index in range(table_size * table_size):
                    dc: dict = cells[index]
                    c: Cell = table.cells[index]
                    c.selected = dc['selected']
                    c.value = dc['value']

                known_tables.append(table)
            
            return known_tables