from abstract import table
from tachando_digitos.cell import Cell

class Table(table.Table):

    def __init__(self, size: int):
        super(Table, self).__init__(size, Cell)
        self.left_list = [-1] * size
        self.top_list = [-1] * size
    
    def pretty_print (self) -> None:
        string_table: str = '   '
        for c in self.top_list:
            string_table += ' | ' + str(c)

        string_table += '|\n'

        for row in range (self.size):
            string_table += str(self.left_list[row]) + ' '
            for col in range (self.size):
                v = self._get_cell(row, col).value
                string_table += ' |  ' + str(v)
            string_table += ' |\n'

        print (string_table)

    def fill_sublist (self, index: int, values: list, sublist_method) -> None:
        """
        param: index -> Integer value to acces to a sublist. It can be a row or a column.
        param: values -> A list<tuple<int, value>> where 'int' refers to a index of the sublist and 'value' refers to the value which will be assign to the cell.
        param: sublist_method -> method where the sublist is got. Those methods are: get_sublist_from_col and get_sublist_from_row.

        return: None
        """
        cs: list = sublist_method (index, 0, self.size - 1)
        sum = 0
        for n in values:
            i = n[0]
            sum += n[1]
            cs[i].value = n[1]
            cs[i].selected = True
    
    def apply_results (self) -> None:
        """
        This method sum all selected cells and assign the result to the corresponding side_list(left_list, top_list)

        return: None
        """
        
        def accu (index: int, method) -> int:
            sublist = method (index, 0, self.size - 1)
            sum: int = 0
            for c in sublist:
                if c.selected:
                    sum += c.value
            
            return sum

        for index in range (self.size):
            self.left_list[index] = accu(index, self.get_sublist_from_row)
            self.top_list[index] = accu (index, self.get_sublist_from_col)
        

    def __eq__(self, t):
        same_left_list: bool = self.left_list == t.left_list
        same_top_list: bool = self.top_list == t.top_list
        same_cells: bool = self._cells == t._cells
        return same_left_list and same_top_list and same_cells
    
    def __ne__(self, t):
        return not (self == t)