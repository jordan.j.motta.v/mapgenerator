class Cell:

    def __init__(self):
        self._value = 0
        self.selected = False
    
    def _set_value(self, val) -> None:
        if val >= 0:
            self._value = val
    
    def _get_value(self) -> int:
        return self._value
    
    value = property(_get_value, _set_value)

    def __eq__(self, c):
        return self.value == c.value
    
    def __ne__(self, c):
        return not self == c
