import unittest
from abstract import table

class TestAbstractTable(unittest.TestCase):

    def setUp(self):
        return super().setUp()

    def test_table_type_instance (self):
        from sutoreto import cell
        t = table.Table(9, cell.Cell)
        # self.assertTrue(t.cells[0] is scell.Cell, 'Las instancia retornada es ' + str(type(t.cells[0])))
        self.assertIsInstance(t.cells[0], cell.Cell, 'Las instancia retornada es ' + str(type(t.cells[0])))
    
    def test_get_cells(self):
        from sutoreto.cell import Cell
        t = table.Table(9, Cell)
        self.assertTrue(len(t.cells) == 9 * 9, 'The size doesn\'t match. Expected {}, but got {} instead.'.format(9*9, len(t.cells)))
    
    def test_table_sublist (self):
        from tachando_digitos import cell
        t = table.Table (9, cell.Cell)
        lc = t.get_sublist_from_col(1, 0, 5)
        self.assertIsInstance (lc, list, 'A list type was expected, but got ' + str(type(lc)))
        self.assertTrue (len(lc) == 6)
        lr = t.get_sublist_from_row(1, 0, 8)
        self.assertIsInstance (lr, list, 'A list type was expected, but got ' + str(type(lr)))
        self.assertTrue (len(lr) == 9, 'Expected length of 4, got ' + str(len(lr)) + ' instead')
    

    def test_get_coord(self):
        from sutoreto.cell import Cell
        t = table.Table(9, Cell)

        coord = (2, 2)
        c1 = t.get_cell(coord[0], coord[1])
        tp = t.get_coord(c1)
        print(tp)
        self.assertTrue(tp == coord, 'c1 is not equal to c2')
    
    def test_cells_to_dict(self):
        from tachando_digitos.cell import Cell
        t = table.Table(9, Cell)
        t.get_cell(0, 6).value = 6
        cells_dict = t.cells_to_dict('value', 'selected')
        cd: dict = cells_dict[6]
        self.assertTrue('value' in cd)
        self.assertTrue('selected' in cd)
        self.assertTrue(len(cells_dict) == 9*9, 'Length doesn\'t match. Expected {}, but got {} instead.'.format(9*9, len(cells_dict)))
        self.assertTrue(cd['value'] == 6)
