import unittest
from tachando_digitos.cell import Cell
from tachando_digitos.table import Table

class TestTachandoTable(unittest.TestCase):

    def setUp(self):
        return super().setUp()
    
    
    
    def test_apply_result(self):
        table = Table(9)
        c: Cell = table.get_cell(3, 4)
        c.value = 7
        c.selected = True
        c = table.get_cell(3, 7)
        c.value = 9
        c.selected = True
        c = table.get_cell(6, 4)
        c.value = 1
        c.selected = True
        table.apply_results()
        self.assertTrue(table.left_list[3] == 16)
        self.assertTrue(table.left_list[6] == 1)
        self.assertTrue(table.top_list[7] == 9)
        self.assertTrue(table.top_list[4] == 8)