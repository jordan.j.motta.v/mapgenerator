import unittest
import random
import json
from tachando_digitos.algorythm import Algorythm
from tachando_digitos.table import Table


class TestTachandoAlgo(unittest.TestCase):

    def setUp(self):
        return super().setUp()
    
    def test_get_sublists (self):
        size: int = 9
        algo = Algorythm()
        t = Table (size)
        rows = algo.split_table_rows(t)
        self.assertIsInstance (rows, list)
        self.assertTrue (len(rows) == size)
        cols = algo.split_table_cols(t)
        self.assertIsInstance (cols, list)
        self.assertTrue (len(cols) == size)
    
    def test_fill_sublist (self):
        SIZE: int = 9
        algo: Algorythm = Algorythm ()
        t = Table (SIZE)
        rows: list = algo.split_table_rows (t)
        cols: list = algo.split_table_cols (t)

        row_index: int = random.randint (1, SIZE - 1)
        row_selected_items: int = algo.fill_row (t, row_index)
        row_counted_items: int = 0

        col_index: int = random.randint (1, SIZE - 1)
        col_selected_items: int = algo.fill_col (t, col_index)
        col_counted_items: int = 0

        for c in cols[col_index]:
            if c.selected == True:
                col_counted_items += 1

        # print ([cell.value for cell in rows[row_index]])
        for c in rows[row_index]:
            if c.selected == True:
                row_counted_items += 1
        
        t.pretty_print ()
        self.assertTrue (row_selected_items == row_counted_items, 'Expected ' + str(row_selected_items) + ' items. Got ' + str(row_counted_items) + ' instead.')
        self.assertTrue (col_selected_items == col_counted_items, 'Expected ' + str(col_selected_items) + ' items. Got ' + str(col_counted_items) + ' instead.')
    
    def test_sum_cells (self):
        SIZE: int = 9
        algo = Algorythm ()
        t = Table (SIZE)
        row_index = random.randint (1, SIZE - 1)
        rows: list = algo.split_table_rows (t)
        algo.fill_row(t, row_index)
        algo.sum_side_list (rows[row_index], t.left_list, row_index)

        outer_sum: int = 0
        for c in rows[row_index]:
            if c.selected:
                outer_sum += c.value
        
        t.pretty_print ()
        self.assertTrue(outer_sum == t.left_list[row_index], 'Expected ' + str(t.left_list[row_index]) + '. Got ' + str(outer_sum) + ' instead.')
    
    def test_generate (self):
        SIZE: int = 9
        algo = Algorythm ()

        ts = algo.generate (SIZE)

        rows: list = algo.split_table_rows (ts[0])

        sublists = rows

        ts[0].pretty_print ()
        ts[1].pretty_print ()

        for n in range (len (sublists)):
            sum: int = 0
            for c in rows[n]:
                sum += c.value
            left_value: int = ts[0].left_list[n]
            self.assertTrue(left_value == sum, 'Expected ' + str(left_value) + '. Got ' + str (sum) + ' instead.')
    
    def test_generate_batch(self):
        size: int = 9
        algo = Algorythm()
        batch = 50
        known_tables = algo.load_tables()
        tables = algo.generate_batch(batch, size, known_tables)
        self.assertTrue(len(tables) == batch, 'The amount doesn\'t fit. Expected {}, but got {}.'.format(batch, len(tables)))
    
    def test_save_tables(self):
        size: int = 9
        algo = Algorythm()
        batch = 10
        tables = algo.generate_batch(batch, size)
        success: bool = algo.save_tables(tables)
        self.assertTrue(success)
    
    def test_load_tables(self):
        algo = Algorythm()
        known_tables = algo.load_tables()
        tables: list = algo.generate_batch(20, 9, known_tables)
        self.assertTrue(len(tables) == 20, 'Couldn\'t load all tables. Expected {}, but got {}'.format(20, len(tables)))