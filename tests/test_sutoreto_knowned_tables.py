import unittest
from sutoreto.knowned_table import KnownedTable

class TestSutoretoKnownedTable(unittest.TestCase):

    def test_load_tables(self):
        kt: KnownedTable = KnownedTable()
        table_count: int = kt.load_tables()

        self.assertTrue(table_count == 2, 'The size of the array of tables doesn\'t match. Expected {}, but got {} instead'.format(2, table_count))
    
    def test_get_table(self):
        kt: KnownedTable = KnownedTable()
        kt.load_tables()
        cs: list = kt.get_table()
        print (cs)
        self.assertTrue(type(cs) == list, 'The type of the returned value isn\'t a list. Expected a list, but got {}'.format(type(cs)))