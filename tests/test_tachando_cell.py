import unittest
from tachando_digitos import cell

class TestCellCrossedOut (unittest.TestCase):

    def setUp(self):
        return super().setUp()

    def test_cell_instance (self):
        c: cell.Cell = cell.Cell ()
        self.assertTrue (c)
    
    def test_cell_change_value (self):
        c = cell.Cell ()

        VALUE: int = 5
        c.value = VALUE
        self.assertTrue (c.value == VALUE, 'El Valor no es igual. Esperando: ' + str(VALUE) + ', pero obtenido: ' + str(c.value))
    
    def test_selected_cell (self):
        c = cell.Cell ()
        c.selected = True
        self.assertTrue (c.selected)
        c.selected = False
        self.assertTrue (not c.selected)