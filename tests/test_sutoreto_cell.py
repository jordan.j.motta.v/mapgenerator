import unittest
import sutoreto.cell as Cell

class TestSutoretoCell (unittest.TestCase):

    def __init__(self, methodName):
        super().__init__(methodName)
        # self.cell = Cell()
    
    def test_set_value (self):
        cell = Cell.Cell()
        VALUE = 5
        cell.value = VALUE
        self.assertEqual (cell.value, 5, "El valor no es el mismo. Value setted: " + str(VALUE) + ", but got: " + str(cell.value))
    
    def test_set_black (self):
        cell = Cell.Cell()
        cell.black = True
        self.assertTrue (cell.black, "No esta tachado")
    
    
    def test_add_possible_values_outside_range (self):
        cell = Cell.Cell()
        self.assertRaises(ValueError, lambda: cell.add_possible_values(10, 11, 12))
    
    def test_eq_cell (self):
        c1 = Cell.Cell()
        c2 = Cell.Cell()
        self.assertTrue (c1 == c2)
    
    def test_ne_cell (self):
        c1 = Cell.Cell()
        c2 = Cell.Cell()
        c1.black = True
        self.assertTrue (c1 != c2)
    