import unittest
from sutoreto.table import Table
from sutoreto.algorythm import Algorythm
# import sutoreto.table as Table

class TestSutoretoTable (unittest.TestCase):

    def setUp(self):
        return super().setUp()

    def test_generate_table (self):
        """
        Generate a table which cells must be have black property as False
        and size must be as specified.
        """
        table = Table(9)
        cells = table.cells
        

        for c in cells:
            self.assertTrue(not c.black)
    
    def test_set_black_cells (self):
        table = Table(9)
        table.set_cell_crossed_out(4, 4, True)
        self.assertTrue(table.is_cell_crossed_out(4, 4))
    
    def test_not_equal_table (self):
        t1 = Table (9)
        t2 = Table (9)
        t2.set_cell_crossed_out (2, 2, True)
        self.assertTrue(t1 != t2)
    
    def test_cells_to_dict(self):
        t: Table = Table(9)

        t.get_cell(0, 0).value = 5
        t.set_cell_crossed_out(0, 2, True)

        a: list = t.cells_to_dict('black', 'value')
        print(a)
    
    def test_extract_sublist(self):
        s: int = 9
        t: Table = Table(s)

        l = t.extract_sublist(0, 0)

        self.assertTrue(len(l) == s, 'The size of the sublist doesn\'t match. Expected {}, but got {}'.format(s, len(l)))

        t.set_cell_crossed_out(0, 5, True)
        l = t.extract_sublist(0, 2)
        print(l)
        self.assertTrue(len(l) == 5, 'The size of the sublist doesn\'t match. Expected {}, but got {}'.format(5, len(l)))
    
    def test_eq_table(self):

        t0: Table = Table(9)
        t1: Table = Table(9)

        self.assertTrue(t0 == t1)
    
    def test_ne_table(self):
        t0: Table = Table(9)
        t1: Table = Table(9)
        t0.get_cell(5, 5).value = 6
        self.assertTrue(t0 != t1)