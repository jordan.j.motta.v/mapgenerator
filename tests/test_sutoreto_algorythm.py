import unittest
from sutoreto.algorythm import Algorythm
from sutoreto.table import Table
from sutoreto.cell import Cell
import traceback

class TestSutoretoAlgorythm (unittest.TestCase):

    def setUp(self):
        return super().setUp()
    
    def test_generate_table (self):
        """
        Generate table with a random configuration.
        In orden to check it, it must be at least a crossed out cell.
        """
        algo = Algorythm ()
        tables = algo.generate (9)
        tables[0].pretty_print()
        print('------------------')
        tables[1].pretty_print()
        self.assertTrue(True)
                
    
    def test_generate_table_batch (self):
        algo = Algorythm ()
        batch = 50
        tables = algo.generate_batch (batch, 9)
        self.assertTrue(len(tables) > 1 , 'No se ha cumplido con el minimo requerido. Especifcada: ' + str(1) + ", pero retorna: " + str(len(tables)))

    def test_cross_table_out (self):
        algo = Algorythm()
        t = Table(9)
        algo.cross_cells_out(t)
        t.pretty_print ()
        self.assertTrue (True)
    
    def test_get_neighbours(self):
        algo = Algorythm()
        t = Table(9)
        
        n = algo.get_neighbours(t, 5,5)

        self.assertTrue(len(n) == 4, 'Expected {}, but got {}'.format(4, len(n)))

        n = algo.get_neighbours(t, 0, 0)

        self.assertTrue(len(n) == 2, 'Expected {}, but got {}'.format(2, len(n)))

        n = algo.get_neighbours(t, 8, 8)

        self.assertTrue(len(n) == 2, 'Expected {}, but got {}'.format(2, len(n)))

        t.set_cell_crossed_out(5, 5, True)
        n = algo.get_neighbours(t, 4, 5)
        self.assertTrue(len(n) == 3, 'Expected {}, but got {}'.format(3, len(n)))
    
    def test_visit_cells(self):
        algo = Algorythm()
        t = Table(9)

        c: Cell = t.get_cell(0, 0)

        visited: Cell = [c]

        algo.visit(t, visited, c)

        self.assertTrue(len(visited) == 9 * 9, 'No se han visitado todas la celdas. Expected: {}, but got: {}'.format(9*9, len(visited)))

        t.set_cell_crossed_out(5, 5, True)

        visited = [c]
        algo.visit(t, visited, c)

        self.assertTrue(len(visited) == (9 * 9) - 1, 'No se han visitado todas la celdas. Expected: {}, but got: {}'.format(9*9 - 1, len(visited)))

        t.set_cell_crossed_out(0, 5, True)
        t.set_cell_crossed_out(1, 5, True)
        t.set_cell_crossed_out(2, 5, True)
        t.set_cell_crossed_out(3, 5, True)
        t.set_cell_crossed_out(4, 5, True)
        t.set_cell_crossed_out(5, 5, True)
        t.set_cell_crossed_out(6, 5, True)
        t.set_cell_crossed_out(7, 5, True)
        t.set_cell_crossed_out(8, 5, True)

        visited = [c]

        algo.visit(t, visited, c)
        EXPECTED: int = 45
        self.assertTrue(len(visited) == EXPECTED, 'No se han visitado todas la celdas. Expected: {}, but got: {}'.format(EXPECTED, len(visited)))
    
    def test_check_solution(self):
        algo = Algorythm()
        t = Table(9)

        blacked = []

        is_solution = algo.check_solution(t, 0, 0, blacked)

        self.assertTrue(is_solution, 'No solution. Expected: {}, but got {}'.format(True, is_solution))

        blacked = []
        t.set_cell_crossed_out(2, 7, True)
        blacked.append(t.get_cell(2, 7))

        is_solution = algo.check_solution(t, 8, 8, blacked)
        self.assertTrue(is_solution, 'No solution. Expected: {}, but got {}'.format(True, is_solution))

        t.set_cell_crossed_out(2, 7, False)
        blacked = []
        t.set_cell_crossed_out(4, 3, True)
        blacked.append(t.get_cell(4, 3))

        is_solution = algo.check_solution(t, 0, 0, blacked)
        self.assertTrue(is_solution, 'No solution. Expected: {}, but got {}'.format(True, is_solution))

        t.set_cell_crossed_out(0, 5, True)
        blacked.append(t.get_cell(0, 5))
        t.set_cell_crossed_out(1, 5, True)
        blacked.append(t.get_cell(1, 5))
        t.set_cell_crossed_out(2, 5, True)
        blacked.append(t.get_cell(2, 5))
        t.set_cell_crossed_out(3, 5, True)
        blacked.append(t.get_cell(3, 5))
        t.set_cell_crossed_out(4, 5, True)
        blacked.append(t.get_cell(4, 5))
        t.set_cell_crossed_out(5, 5, True)
        blacked.append(t.get_cell(5, 5))
        t.set_cell_crossed_out(6, 5, True)
        blacked.append(t.get_cell(6, 5))
        t.set_cell_crossed_out(7, 5, True)
        blacked.append(t.get_cell(7, 5))
        t.set_cell_crossed_out(8, 5, True)
        blacked.append(t.get_cell(8, 5))
    
        is_solution = algo.check_solution(t, 0, 0, blacked)
        self.assertTrue(not is_solution, 'No solution. Expected: {}, but got {}'.format(False, is_solution))
    
    def test_split_list(self):
        algo = Algorythm()
        t: Table = Table(9)

        t.set_cell_crossed_out(0, 3, True)
        t.set_cell_crossed_out(0, 6, True)

        row0: list = t.get_sublist_from_row(0, 0, 8)
        sublists: list = algo._split_list(row0)

        self.assertTrue(len(sublists) == 3, 'Generated sublists doesn\'t match. Expected 3, but got {}'.format(len(sublists)))

        t.set_cell_crossed_out(0, 4, True)
        self.assertTrue(len(sublists) == 3, 'Generated sublists doesn\'t match. Expected 3, but got {}'.format(len(sublists)))


    def test_save_table(self):
        algo = Algorythm()
        tables: list = algo.generate_batch(30, 9)

        success: bool = algo.save_tables(tables)

        self.assertTrue(success, 'Cannot save tables')