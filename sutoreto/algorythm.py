import random
import copy
import json
import os
from abstract import algorythm
from sutoreto.table import Table
from sutoreto.cell import Cell
from sutoreto.knowned_table import KnownedTable

class Algorythm(algorythm.Algorythm):

    def __init__(self):
        self.visited_cells = []
        self.kt = KnownedTable()
        self.kt.load_tables()
    
    def cross_cells_out (self, table: Table) -> None:
        """
        Set as black a random cells number between 40% and 60% doing backtracking
        """
        MIN: int = int ((table.size * table.size) * 0.4)
        MAX: int = int ((table.size * table.size) * 0.6)
        black_cells: int = random.randint(MIN, MAX)
        bcs: list = []
        for _ in range (black_cells):
            row: int = random.randint (0, table.size - 1)
            col: int = random.randint (0, table.size - 1)
            table.set_cell_crossed_out(row, col, True)
            c: Cell = table.get_cell (row, col)
            bcs.append (c)
            coord: tuple = table.find_free_coord ()
            is_solution = self.check_solution (table, coord[0], coord[1], bcs)
            if is_solution:
                print("It is solution")
                table.set_cell_crossed_out (row, col, False)
            else:
                bcs.remove (c)
                print("It is not solution") 

    def get_neighbours (self, table: Table, row: int, col:int) -> list:
        """
            | x |
        | x | - | x |
            | x |

        Get the neighbours of '-' cell.
        These neighbours 
        """

        indexes = [
            (row, col - 1), # Left
            (row - 1, col), # Top
            (row, col + 1), # Right
            (row + 1, col), # Bottom
        ]

        cs = []
        for index in indexes:
            c: Cell = table.get_cell(index[0], index[1])
            if c is not None:
                if not c.black:
                    cs.append (c)
        
        return cs
    
    def visit (self, table: Table, visited: list, cell: Cell) -> None:
        row, col = table.get_coord(cell)

        neighbours = self.get_neighbours(table, row, col)

        for n in neighbours:
            # Check whether the cell is in list or not.
            exists: bool = False
            for c in visited:
                exists |= n is c
            if not exists:
                visited.append(n)
                self.visit(table, visited, n)


    def config_possible_values(self, table: Table, crossed_lists: list) -> None:
        """
        Config possible values from the sublist inside crossed_list.
        """
        if crossed_lists[0] > 1:
            pass
        if crossed_lists[1] > 1:
            pass
    

    def check_solution (self, table: Table, row: int, col: int, black_cells: list) -> bool:
        """
            It gets how many black and no black cells might count.
            If the sum of those cells is equal to the table's size, is solution, is not solution otherwise.
        """
        cell: Cell = table.get_cell(row, col)
        visited = [cell]

        self.visit(table, visited, cell)

        total: int = len(black_cells) + len(visited)
        return total == table.size * table.size
    
    def _split_list(self, l: list) -> list:
        sublists: list = []
        sl: list = []
        for c in l:
            if not c.black:
                sl.append(c)
            else:
                if len(sl) != 0:
                    sublists.append(sl)
                sl = []
        sublists.append(sl)
        # Remove list wioth len == 1
        for index in range(len(sublists)):
            if len(sublists[index]) == 1:
                sublists.pop(index) 
        return sublists
    
    def _get_values_left(self, table: Table, row: int, col: int) -> set:
        co: int = col
        values = set()
        while True:
            c: Cell = table.get_cell(row, co)
            # print (c)
            if c is None:
                break
            elif c.black:
                break
            else:
                co -= 1
            if c.value is None:
                print("None Value")
                continue
            values.add(c.value)
        return values
    
    def _get_values_right(self, table: Table, row: int, col: int) -> set:
        co: int = col
        values = set()
        while True:
            c: Cell = table.get_cell(row, co)
            # print (c)
            if c is None:
                break
            elif c.black:
                break
            else:
                co += 1
            if c.value is None:
                print("None Value")
                continue
            values.add(c.value)
        return values
    
    def _get_values_top(self, table: Table, row: int, col: int) -> set:
        values = set()
        while True:
            c: Cell = table.get_cell(row, col)
            if c is None:
                break
            elif c.black:
                break
            else:
                row -= 1
            if c.value is None:
                print("None Value")
                continue
            values.add(c.value)
        return values

    def _get_valid_value(self, values: set) -> int:
        min: int = 9
        max: int = 0
        for v in values:
            if min >= v:
                min = v - 1
            if max <= v:
                max = v + 1
        possibles = [min, max]
        index: int = random.randint(0, 1)
        return possibles[index]

    def fill_numbers(self, table: Table, row: int, col: int) -> int:
        """
        params:
            row: int -> This row must be valid for a cell.
            col: int -> This col must be valid for a cell .
            NOTE: "Valid cell" is a cell which black property is False.
        """
        if row >= table.size:
            return 0
        if row == 0:
            coords: tuple = table.extract_sublist(row, col)
            for coord in coords:
                r: int = coord[0]
                c: int = coord[1]
                values: set = self._get_values_left(table, r, c - 1)
                # print(values)
                valid_value: int = 0
                if len(values) != 0:
                    # print ("previous")
                    valid_value: int = self._get_valid_value(values)
                else:
                    print ("Random")
                    valid_value = random.randint(0, 9)
                table.get_cell(r, c).value = valid_value
            for coord in coords:
                r: int = coord[0]
                c: int = coord[1]
                cell: Cell = table.get_cell(r + 1, c)
                if not cell.black and cell.value is None:
                    self.fill_numbers(table, r + 1, c)
        elif row == table.size - 1:
            coords: tuple = table.extract_sublist(row, col)
            for coord in coords:
                r: int = coord[0]
                c: int = coord[1]
                # Check previous values in lower rows
                values_top: set = self._get_values_top(table, r - 1, c)
                values_left: set = self._get_values_left(table, r, c - 1)
                values_right: set = self._get_values_left(table, r, c + 1)
                values: set = values_left | values_top | values_right
                # print(values)
                valid_value: int = 0
                if len(values) != 0:
                    # print ("previous")
                    valid_value: int = self._get_valid_value(values)
                else:
                    print ("Random")
                    valid_value = random.randint(0, 9)
                table.get_cell(r, c).value = valid_value
            return 0
        else:
            next_coord: tuple = None
            coords: tuple = table.extract_sublist(row, col)
            for coord in coords:
                r: int = coord[0]
                c: int = coord[1]
                # Check previous values in lower rows
                values_top: set = self._get_values_top(table, r - 1, c)
                values_left: set = self._get_values_left(table, r, c - 1)
                values_right: set = self._get_values_left(table, r, c + 1)
                values: set = values_left | values_top | values_right
                # print(values)
                valid_value: int = 0
                if len(values) != 0:
                    # print ("previous")
                    valid_value: int = self._get_valid_value(values)
                else:
                    print ("Random")
                    valid_value = random.randint(0, 9)
                table.get_cell(r, c).value = valid_value
            for coord in coords:
                r: int = coord[0]
                c: int = coord[1]
                cell: Cell = table.get_cell(r + 1, c)
                if not cell.black and cell.value is None:
                    self.fill_numbers(table, r + 1, c)
        return 0

    def generate_lists(self, table: Table) -> list:
        """
            Split the table in rows and cols.
            Those lists, in turn, will be splited in sublist when the delimiter
            is a blacked cell.
        """

        sublists: list = []
        rows: list = self.split_table_rows(table)
        for row in rows:
            pass
        cols: list = self.split_table_cols(table)
        pass

    def set_cells(self, table: Table, cs_positions: list) -> None:
        for coord in cs_positions:
            x: int = coord[0]
            y: int = coord[1]
            c: Cell = table.get_cell(x, y)
            c.selected = True
    
    
    def config_table(self, table: Table) -> Table:
        copy_table: Table = copy.deepcopy(table)
        copy_cells = [c for c in copy_table.cells if c.selected]
        hide_cells = len(copy_cells)

        for _ in range(hide_cells):
            index: int = random.randint(0, len(copy_cells) - 1)
            copy_cells[index].value = None
        return copy_table


    def set_blacks(self, table: Table) -> None:
        for c in table.cells:
            if not c.selected:
                c.black = True

    def generate(self, size: int) -> tuple:
        t: Table = Table(size)
        cs_positions: list = self.kt.get_table()
        self.set_cells(t, cs_positions)
        self.set_blacks(t)
        row: int = cs_positions[0][0]
        col: int = cs_positions[0][1]
        self.fill_numbers(t, row, col)
        cp: Table = self.config_table(t)
        return (t, cp)

    def save_tables(self, tables: list) -> bool:
        with open('sutoreto_tables.json', 'a') as outfile:
            l = []
            for t in tables:
                t0: Table = t[0]
                cs0: list = t0.cells_to_dict('selected', 'value', 'black')
                t0vals: dict = {
                    'cells': cs0
                }
                t1: Table = t[1]
                cs1 = t1.cells_to_dict('selected', 'value', 'black')
                t1vals: dict = {
                    'cells': cs1
                }

                size: int = t0.size

                vals = {
                    'size': size,
                    'answer_table': t0vals,
                    'complete_table': t1vals
                }
                l.append(vals)
            json.dump(l, outfile)
        return True
    
    def load_tables(self):
        """
        Load tables from json file
        """
        filename: str = 'sutoreto_tables.json'
        known_tables: list = []
        if not os.path.exists(filename):
            return known_tables
        with open(filename) as output:
            tables: list = json.load(output)
            for ca_table in tables:
                ct: dict = ca_table['complete_table']
                size: int = ca_table['size']
                t: Table = Table(size)
                cells = ct['cells']
                for index in range(size * size):
                    cd: dict = cells[index]
                    c: Cell = t.cells[index]
                    c.selected = cd['selected']
                    c.black = cd['black']
                    c.value = cd['value']
                known_tables.append(t)
            
            return known_tables

    
    def generate_batch (self, batch: int, table_size: int, known_tables: list = []) -> list:
        l = []
        batch_counter: int = 0
        while batch_counter < batch:
            try:
                ts: tuple = self.generate(table_size)
                if not self.exists(ts[0], known_tables):
                    l.append(ts)
                    batch_counter += 1
                    known_tables.append(ts[0])
            except:
                print ('A ocurrido un error generando la tabla')
        return l
