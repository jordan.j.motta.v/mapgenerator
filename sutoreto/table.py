from abstract import table
from sutoreto.cell import Cell

class Table(table.Table):

    def __init__(self, size: int):
        super(Table, self).__init__(size, Cell)
        self.selected_cells = 0

    def _generate_cells(self) -> list: # Deprecated
        l = []
        for _ in range(self._size * self._size):
            l.append (Cell())
        return l

    def is_cell_crossed_out (self, row: int, col: int) -> bool:
        return self._get_cell(row, col).black
    
    def set_cell_crossed_out (self, row: int, col: int, crossed: bool) -> None:
        self._get_cell (row, col).black = crossed

    def assign_value (self, row: int, col: int, value: int) -> None:
        self._get_cell(row, col).value = value
    
    def get_cell(self, row, col):
        if row < 0 or row >= self.size or col < 0 or col >= self.size:
            return None
        return super().get_cell(row, col)
    
    def find_free_coord(self) -> tuple:
        """
        Look for cell which black property is False.
        """
        for c in self.cells:
            if not c.black:
                return self.get_coord(c)
    
    def get_available_cell(self) -> Cell:
        """
        Returns a cell which is not taged as black.
        """
        for c in self.cells:
            if not c.black:
                return self.get_coord(c)
        return None
    
    def get_list(self, row: int, col: int) -> list:
        """
        Returns two sublists inside another one. Those sublists
        go from a side or blacked cell to another side or blacked cell which
        pivot is is the cell specified with 'row' and 'col'.
        """

        # from_row_index: int = row
        # blacked_cell: bool = False
        # while True:
        #     blacked_cell = self.is_cell_crossed_out(from_row_index, col)
        #     if blacked_cell:
        #         break
        #     if from_row_index - 1 < 0:
        #         break
        #     from_row_index -= 1
        
        # if blacked_cell:
        #     from_row_index += 1

        # blacked_cell = False
        # sub_col: list = []
        # while True:
        #     blacked_cell = self.is_cell_crossed_out(from_row_index, col)
        #     if blacked_cell:
        #         break
        #     else:
        #         c: Cell = self.get_cell(from_row_index, col)
        #         sub_col.append(c)
        #         if from_row_index + 1 >= self.size:
        #             break
        #         from_row_index += 1

        from_col_index: int = col
        blacked_cell: bool = False
        while True:
            blacked_cell = self.is_cell_crossed_out(row, from_col_index)
            if blacked_cell:
                break
            if from_col_index - 1 < 0:
                break
            from_col_index -= 1
        
        if blacked_cell:
            from_col_index += 1

        blacked_cell = False
        sub_row: list = []
        while True:
            blacked_cell = self.is_cell_crossed_out(row, from_col_index)
            if blacked_cell:
                break
            else:
                # c: Cell = self.get_cell(row, from_col_index)
                sub_row.append((row, from_col_index))
                if from_col_index + 1 >= self.size:
                    break
                from_col_index += 1
        
        return sub_row
    
    def extract_sublist(self, row: int, col: int) -> list:
        while True:
            c: Cell = self.get_cell(row, col)
            if c is None: # coord is out of bounds.
                break
            elif c.black:
                break
            col -= 1
        
        col += 1

        l = []
        while True:
            c: Cell = self.get_cell(row, col)
            if c is None:
                break
            elif c.black:
                break
            l.append((row, col))
            col += 1

        return l

    def pretty_print (self):
        for row in range (self._size):
            srow = ''
            for col in range (self._size):
                c = self.get_cell (row, col)
                val = '-' if not c.black else 'x'
                val = '+' if c.selected else val
                val = str(c.value) if c.value is not None else val
                srow += ' | ' + val
            srow += ' |'
            print(srow)

    def __ne__ (self, other):
        return not (self == other)
    
    def __eq__ (self, other):
        if self.size != other.size:
            return False
        else:
            cells1 = self.cells
            cells2 = other.cells
            for i in range(self.size * self.size):
                if cells1[i] != cells2[i]:
                    return False
            return True
    
