import json
import random

class KnownedTable:
    """
    Table with preconfigured cell location in order to simulate
    random tables.

    Those cell positions are located in a json file.

    json file structure: [
        [
            (x0, y0), # cell position
            (x1, y1),
            ...
            (xn, yn)
        ] # table
    ]
    """

    def __init__(self):
        self.tables = []
        self.size = len(self.tables)

    def load_tables(self) -> int:
        """
        load the tables inside a json file.

        return int: The number of tables inside json file.
        """
        with open('sutoreto/knowned_table_cell_position.json') as f:
            self.tables = json.load(f)
            self.size = len(self.tables)
            return self.size
    
    def get_table(self) -> list:
        """
        Get a random table solution.
        """
        r = random.randint(0, self.size - 1)
        return self.tables[3]

            
