class Cell:
    MAX_VALUE = 9
    MIN_VALUE = 0

    def __init__(self):
        self._possible_values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        self._black = False
        self._value = None
        self.selected = False

    def _get_value(self):
        return self._value

    def _set_value(self, val) -> None:
        if self._black:
            return
        if val in self.possible_values or val is None:
            self._value = val

    value = property(_get_value, _set_value)

    def _set_black(self, value: bool) -> None:
        self._black = value

    def _is_black(self) -> bool:
        return self._black

    black = property(_is_black, _set_black)

    def _get_possible_values(self) -> list:
        return self._possible_values
    
    possible_values = property(_get_possible_values)

    def remove_possible_values (self, *args):
        for v in args:
            self._possible_values.discard(v)
    
    def add_possible_values (self, *args):
        for v in args:
            if v < self.MIN_VALUE or v > self.MAX_VALUE:
                raise ValueError("Intentando agregar un valor fuera del rango")
            self._possible_values.add (v)
    
    def get_possible_values (self):
        """
            Return
        """
        return self._possible_values
    
    possible_values = property (get_possible_values)

    def __eq__ (self, other):
        if self.black != other.black:
            return False
        else:
            if self.black:
                return True
            else:
                return self.value == other.value
    
    def __ne__ (self, other):
        return not (self == other)